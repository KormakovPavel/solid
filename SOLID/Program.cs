﻿using System;

namespace SOLID
{
    class Program
    {
        static void Main()
        {
            var triangle = new Triangle(5,10,4,4);
            var cicrle = new Circle(10);            
            Figure.Square(triangle);
            Figure.Perimeter(cicrle);

            triangle.FigureColor = "Red";
            cicrle.FigureColor = "Blue";

            Console.WriteLine($"Цвет треугольника: {triangle.FigureColor}");
            Console.WriteLine($"Цвет круга: {cicrle.FigureColor}");            

            Console.ReadLine();
        }
    }
}
