﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID
{
    interface IPerimeter
    {
        double GetPerimeter();
    }
}
