﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID
{
    class Circle : ISquare, IPerimeter, IFigureСolor
    {
        private readonly int radius;
        public string FigureColor { get; set; }
        public Circle(int _radius)
        {
            radius = _radius;
        }
        public double GetSquare() => Math.PI * Math.Pow(radius, 2);
        public double GetPerimeter() => 2 * Math.PI * radius;
    }
}
