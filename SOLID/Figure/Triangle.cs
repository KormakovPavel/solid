﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID
{
    class Triangle : ISquare, IPerimeter, IFigureСolor
    {
        private readonly int sideA;
        private readonly int sideB;
        private readonly int height;
        private readonly int widthBase;
        public string FigureColor { get; set; }
        public Triangle(int _height, int _widthBase, int _sideA, int _sideB)
        {
            height = _height;
            widthBase = _widthBase;
            sideA = _sideA;
            sideB = _sideB;
        }       

        public double GetSquare() => 0.5 * height * widthBase;

        public double GetPerimeter() => sideA + sideB + widthBase;
               
    }
}
