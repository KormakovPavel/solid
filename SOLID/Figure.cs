﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID
{
    static class Figure
    {  

        public static void Square(ISquare square)
        {
            Console.WriteLine($"Площадь: {square.GetSquare()}");
        }
        public static void Perimeter(IPerimeter perimeter)
        {
            Console.WriteLine($"Периметр: {perimeter.GetPerimeter()}");
        }
    }
}
